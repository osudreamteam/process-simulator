//
// Created by lkmfwe on 16.09.16.
//

#ifndef PROCESS_SIMULATOR_PROCESSSIMULATOR_H
#define PROCESS_SIMULATOR_PROCESSSIMULATOR_H

#include <vector>
#include <iostream>
#include "Process.h"
#include "util/Logger.h"

class ProcessManager;
class ProcessManagementAlgorithm;

class ProcessSimulator {
protected:
    ProcessManagementAlgorithm * algorithm;
    ProcessManager * processManager;

    uint32 currentTick = 0;
    bool stopped = true;

    uint32 execMaxTick;
public:
    Logger * log;

    ProcessSimulator(ProcessManagementAlgorithm * algorithm, uint32 spawnMaxTick=2000, uint32 execMaxTick=1000);
    ~ProcessSimulator();

    void run();
    void activateProcess(Process *);
    void deactivateProcess(Process *);

    void stop() {
        stopped = true;
    }
    uint32 getCurrentTick() { return currentTick; }
    ProcessManagementAlgorithm * getAlgorithm() {
        return algorithm;
    }

    double getAverageExecTime();
    double getAverageWaitTime();

    void tearDown();
};


#endif //PROCESS_SIMULATOR_PROCESSSIMULATOR_H
