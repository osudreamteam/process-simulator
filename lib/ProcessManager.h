#ifndef PROCESS_SIMULATOR_PROCESSSPAWNER_H
#define PROCESS_SIMULATOR_PROCESSSPAWNER_H


#include <cstdlib>
#include <queue>
#include <cstring>
#include <ctime>
#include <limits>
#include "Process.h"
#include "ProcessSimulator.h"
#include "util.h"

class ProcessManager {
    static const uint16 PROCESS_POOL_SIZE = 100;
    std::queue<uint16> pid_pool;

    uint32 procLifeTime[PROCESS_POOL_SIZE];
    uint32 procCreatedAt[PROCESS_POOL_SIZE];
    uint32 procTimeLeft[PROCESS_POOL_SIZE];
    std::vector<Process *> processes;
    Process * activeProcess;

    uint32 maxWaitTime = 0;
    uint32 minWaitTime = std::numeric_limits<uint32>::max();

    uint32 processExecTime = 0;
    uint32 processWaitTime = 0;
    uint32 processTotalCount = 0;

    void spawnProcess(ProcessSimulator *processSimulator);
    void despawnProcess(Process * process, ProcessSimulator *processSimulator);

public:
    uint32 spawnMaxTick;

    ProcessManager(uint32 spawnMaxTick=2000) : processExecTime(0), processWaitTime(0), processTotalCount(0), activeProcess(nullptr) {
        this->spawnMaxTick = spawnMaxTick;
        unsigned int seed = (unsigned int)time(NULL);
        seed = 1; // TODO: remove this
        srand(seed);
        for (uint16 i = 0; i < PROCESS_POOL_SIZE; ++i)
            pid_pool.push(i);
        memset(procTimeLeft, 0, sizeof(procTimeLeft));
    }

    ~ProcessManager() {
        for (auto process : processes) {
            delete process;
        }
    }

    void tick(ProcessSimulator * processSimulator);

    bool hasFinished(ProcessSimulator * processSimulator);

    void setActiveProcess(Process * process) {
        if (activeProcess)
            activeProcess->status = Process::Status::READY;
        activeProcess = process;
        if (activeProcess)
            activeProcess->status = Process::Status::RUNNING;
    }

    Process * getActiveProcess() {
        return activeProcess;
    }

    uint32 getProcessTimeLeft(uint16 pid) {
        return procTimeLeft[pid];
    }

    uint32 getProcessExecTime() const;
    uint32 getProcessWaitTime() const;
    uint32 getMinWaitTime() const;
    uint32 getMaxWaitTime() const;
    uint32 getProcessTotalCount() const;
};


#endif //PROCESS_SIMULATOR_PROCESSSPAWNER_H
