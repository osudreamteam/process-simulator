#include <iostream>
#include <iomanip>
#include <stdexcept>
#include "ProcessSimulator.h"
#include "ProcessManagementAlgorithm.h"
#include "ProcessManager.h"

void ProcessSimulator::run() {
    stopped = false;
    while(!stopped) {
        processManager->tick(this); // шаг менеджера процессов
        algorithm->tick(this); // шаг алгоритма

        currentTick++; // увеличиваем кол-во тиков
        if (currentTick > this->execMaxTick || processManager->hasFinished(this)) {
            tearDown(); // скатываемся
        }
    }
}

void ProcessSimulator::tearDown() {
    log->logEvent("Stopping...", currentTick);
    stop();
    log->logEvent("Stopped.", currentTick);

    uint32 processTotalCount = processManager->getProcessTotalCount();
    log->logStats("AVG EXEC TIME", std::to_string(getAverageExecTime()));
    log->logStats("AVG WAIT TIME", std::to_string(getAverageWaitTime()));
    log->logStats("MIN WAIT TIME", std::to_string(processManager->getMinWaitTime()));
    log->logStats("MAX WAIT TIME", std::to_string(processManager->getMaxWaitTime()));
}

// TODO: what statuses do ?
void ProcessSimulator::activateProcess(Process * proc) {
    if (proc == nullptr) {
        throw std::invalid_argument(std::string("Process is null"));
    }

    Process * activeProcess = processManager->getActiveProcess();
    if (activeProcess)
        deactivateProcess(activeProcess);

    log->logEvent("Activating process PID: " + std::to_string(proc->pid), currentTick);
    processManager->setActiveProcess(proc);
}

void ProcessSimulator::deactivateProcess(Process * proc) {
    if (proc == nullptr) {
        throw std::invalid_argument(std::string("Process is null"));
    }
    log->logEvent("Deactivating process PID: " + std::to_string(proc->pid) + ", Process ticks left: " +
                          std::to_string(processManager->getProcessTimeLeft(proc->pid)),
                  currentTick);
    processManager->setActiveProcess(nullptr);
}

ProcessSimulator::~ProcessSimulator() {
    delete processManager;
    delete log;
}

ProcessSimulator::ProcessSimulator(ProcessManagementAlgorithm * algorithm, uint32 spawnMaxTick, uint32 execMaxTick) {
    if (algorithm == nullptr)
        throw std::invalid_argument(std::string("Algorithm is null!"));

    this->execMaxTick = execMaxTick;
    this->algorithm = algorithm;
    stopped = true;
    processManager = new ProcessManager(spawnMaxTick);
    log = new Logger("process-simulator.txt");
}

double ProcessSimulator::getAverageExecTime() {
    if (stopped) {
        return processManager->getProcessExecTime()*1.0/processManager->getProcessTotalCount();
    }
    return 0;
}

double ProcessSimulator::getAverageWaitTime() {
    if (stopped) {
        return processManager->getProcessWaitTime()*1.0/processManager->getProcessTotalCount();
    }
    return 0;
}
