#ifndef PROCESS_SIMULATOR_PROCESS_H
#define PROCESS_SIMULATOR_PROCESS_H


#include "util.h"
#include "common.h"

class Process {
    const int PRIORITY_STEP = 1;
public:
    typedef enum {
        BIRTH = 0,
        READY = 1,
        RUNNING = 2,
        WAITING = 4,
        DEATH = 8
    } Status;

    uint16 pid;
    char priority;
    UserType user;
    Status status;
    uint32 startedAt;

    Process * setStatus(Status status) {
        this->status = status;
        return this;
    }
    Status getStatus() { return status; }

    bool increasePriority() {
        if (this->priority > 255 - PRIORITY_STEP)
            return false;
        this->priority += PRIORITY_STEP;
        return true;
    }

    bool decreasePriority() {
        if (this->priority < PRIORITY_STEP)
            return false;
        this->priority -= PRIORITY_STEP;
        return true;
    }
};


#endif //PROCESS_SIMULATOR_PROCESS_H
