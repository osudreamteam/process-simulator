//
// Created by lkmfwe on 12.12.16.
//

#ifndef PROCESS_SIMULATOR_LOGGER_H
#define PROCESS_SIMULATOR_LOGGER_H

#include <string>
#include <fstream>

class Logger {

protected:
    std::ofstream out;

    std::string formatEvent(std::string message, int tick) {
        return "[Tick " + std::to_string(tick) + "] " + message + "\n";
    }

    std::string formatStats(std::string name, std::string message) {
        return "[Stats " + name + "] " + message + "\n";
    }

public:

    Logger(std::string filename) {
        out.open(filename);
    }

    ~Logger() {
        flush();
        out.close();
    }

    virtual void logEvent(std::string message, int tick) {
        out << formatEvent(message, tick);
    }

    virtual void logStats(std::string name, std::string message) {
        out << formatStats(name, message);
    }

    void flush() {
        out.flush();
    }

};


#endif //PROCESS_SIMULATOR_LOGGER_H
