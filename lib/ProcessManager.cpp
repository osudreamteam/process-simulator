//
// Created by lkmfwe on 24.09.16.
//

#include "ProcessManager.h"
#include "Process.h"
#include "ProcessManagementAlgorithm.h"
#include <algorithm>

void ProcessManager::tick(ProcessSimulator * processSimulator) {
    if (activeProcess) {
        processExecTime++;
        if (--procTimeLeft[activeProcess->pid] <= 0) {
            auto activeProcessIterator = std::find(processes.begin(), processes.end(), activeProcess);
            processes.erase(activeProcessIterator);
            despawnProcess(activeProcess, processSimulator);
        }
    }
    processWaitTime += processes.size()-1;

    if (rand()%10000 <= 2222 && !pid_pool.empty() && processSimulator->getCurrentTick() <= spawnMaxTick) {
        spawnProcess(processSimulator);
    }
}

void ProcessManager::despawnProcess(Process *process, ProcessSimulator *processSimulator) {
    auto algorithm = processSimulator->getAlgorithm();

    processSimulator->log->logEvent("Process PID: " + std::to_string(process->pid) + " destroyed",
                                    processSimulator->getCurrentTick());

    uint32 waitTime = processSimulator->getCurrentTick() - procCreatedAt[process->pid] - procLifeTime[process->pid];
    minWaitTime = std::min(minWaitTime, waitTime);
    maxWaitTime = std::max(maxWaitTime, waitTime);

    process->setStatus(Process::DEATH);
    if (algorithm != nullptr)
        algorithm->processDestroyed(process, processSimulator);
    pid_pool.push(process->pid);
    delete process;
}

void ProcessManager::spawnProcess(ProcessSimulator *processSimulator) {
    Process * process = new Process();
    process->pid = pid_pool.front();
    pid_pool.pop();
    process->priority = (char)(std::rand()%256);
    process->startedAt = processSimulator->getCurrentTick();
    process->status = Process::Status::BIRTH;
    process->user = std::rand()%3 ? UserType::SYSTEM : UserType::USER;
    procTimeLeft[process->pid] = (uint32)(std::rand()%1000 + 20);
    procLifeTime[process->pid] = procTimeLeft[process->pid];
    procCreatedAt[process->pid] = processSimulator->getCurrentTick();
    processes.push_back(process);

    auto algorithm = processSimulator->getAlgorithm();

    processSimulator->log->logEvent("Process PID: " + std::to_string(process->pid) + " created",
                                    processSimulator->getCurrentTick());

    if (algorithm)
        algorithm->processCreated(process, processSimulator);
    processTotalCount++;
}

bool ProcessManager::hasFinished(ProcessSimulator *processSimulator) {
    bool res = processSimulator->getCurrentTick() > spawnMaxTick && processes.empty();
    return res;
}

uint32 ProcessManager::getProcessExecTime() const {
    return processExecTime;
}

uint32 ProcessManager::getProcessWaitTime() const {
    return processWaitTime;
}

uint32 ProcessManager::getProcessTotalCount() const {
    return processTotalCount;
}

uint32 ProcessManager::getMaxWaitTime() const {
    return maxWaitTime;
}

uint32 ProcessManager::getMinWaitTime() const {
    return minWaitTime;
}
