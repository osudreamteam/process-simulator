#ifndef PROCESS_SIMULATOR_PROCESSMANAGEMENTALGORITHM_H
#define PROCESS_SIMULATOR_PROCESSMANAGEMENTALGORITHM_H


#include "Process.h"
#include "ProcessSimulator.h"

class ProcessManagementAlgorithm {
public:
    virtual void processCreated(Process *, ProcessSimulator *) = 0;
    virtual void processDestroyed(Process *, ProcessSimulator *) = 0;
    virtual void tick(ProcessSimulator *) = 0;
};


#endif //PROCESS_SIMULATOR_PROCESSMANAGEMENTALGORITHM_H
