#include <iostream>
#include <queue>
#include "lib/ProcessManagementAlgorithm.h"

class QueueAlgorithm : public ProcessManagementAlgorithm {
    std::queue<Process *> q;
    int curRunTime = 0;
    uint32 processQuant;
    Process * activeProcess = nullptr;
public:
    QueueAlgorithm(uint32 processQuant=5) {
        this->processQuant = processQuant;
    }

    void processCreated(Process * proc, ProcessSimulator * procSim) {
//        std::cout << "Process created, pid: " << proc->pid << std::endl;
        q.push(proc);
    }

    void processDestroyed(Process * proc, ProcessSimulator * procSim) {
//        std::cout << "Process destroyed, pid: " << proc->pid << std::endl;
        q.pop();
        activeProcess = nullptr;
        nextProcess(procSim);
    }

    void tick(ProcessSimulator * procSim) {
        if (activeProcess) {
            curRunTime++;
            if (curRunTime >= processQuant)
                nextProcess(procSim);
        }
        else
            nextProcess(procSim);
        //std::cout << "Tick: " << procSim->getCurrentTick() << std::endl;
    }

    void nextProcess(ProcessSimulator * procSim) {
        if (q.empty()) {
            activeProcess = nullptr;
        }
        else {
            if (activeProcess) {
                q.pop();
                q.push(activeProcess);
            }
            if (!q.empty()) {
                activeProcess = q.front();
                procSim->activateProcess(activeProcess);
            } else
                activeProcess = nullptr;
            curRunTime = 0;
        }
    }
};

int main() {
    ProcessManagementAlgorithm *qa = new QueueAlgorithm(11);
    ProcessSimulator procSim(qa, 10000, 100000);

    procSim.run();

    delete qa;

    return 0;
}